<?php
class indexView implements IView {
    
    private $controller;
    
    public function __construct($controller) {
        $this->controller = $controller;
    }
    
    public function index() {
        return new View("Hello", 'all');
    }
    
    public function profile() {
        return new View(TempData::GetTempData('test'), 'all');
    }
}