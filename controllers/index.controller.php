<?php
class indexController implements IController {
    
    public static $username;
    
    public function Process($args) {
        $this->username = rand();
    }
    
    public function index() {
        $this->username = 'index';
    }
    
    public function profile() {
        TempData::SetTempData('test', rand());
    }
    
    public function logout() {
        Account::Logout();
        
        Request::Redirect('?page=index&command=index');
    }
    
    public function RequiredAuth() {
        return 0;
    }
}