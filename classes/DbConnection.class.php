<?php
class DbConnection {
    
    private $pdo;
    
    public function __construct() {
        try
        {
            if($this->pdo==null){
                
             $fn = realpath('C:\xampp\htdocs\Note\data\notes.sqlite');
                
            $this->pdo =new PDO("sqlite:".$fn,"","",array(
                    PDO::ATTR_PERSISTENT => true
                ));
            }
        }catch(PDOException $e){
            print "Error in openhrsedb ".$e->getMessage();
        }
    }
    
    public function AddUser($email, $password, $voornaam, $achternaam) {
        try {
            $stmt = $this->pdo->prepare('INSERT INTO users(password, voornaam, achternaam, email) VALUES(?,?,?,?);');
            $stmt->execute(array(password_hash($password, PASSWORD_DEFAULT), $voornaam, $achternaam, $email));
            return true;
        } catch(Exception $e) {
            return false;
        } 
    }
    
    public function GetUsers() {
        $stmt = $this->pdo->prepare("SELECT * FROM users");
        $stmt->execute();
        $result = $stmt->fetchAll();
        
        return $result;
    }
    
    public function VerifyUser($email, $password) {
        $stmt = $this->pdo->prepare("SELECT * FROM users WHERE email = ? LIMIT 1");
        $stmt->execute(array($email));
        $result = $stmt->fetchAll();
        
        if(count($result) < 1)
            return false;
        
        $pw = $result[0]['password'];
        
        return password_verify($password, $pw);
    }
    
    public function GetUser($email) {
        $stmt = $this->pdo->prepare("SELECT * FROM users WHERE email = ? LIMIT 1");
        $stmt->execute(array($email));
        $result = $stmt->fetchAll();
        
        if(count($result) < 1)
            return false;
            
        return $result[0];
    }
    
    public function AddNote($email, $title, $note) {
        $user = $this->GetUser($email);
        
        $stmt = $this->pdo->prepare("INSERT INTO notes(title, text, user_id) VALUES(?,?,?);");
        $stmt->execute(array($title, $note, $user['id']));
    }
    
    public function GetNotes($email) {
        $user = $this->GetUser($email);
        
        $stmt = $this->pdo->prepare("SELECT * FROM notes WHERE user_id = ?;");
        $stmt->execute(array($user['id']));
        
        return $stmt->fetchAll();
    }
}