<?php
class View {
    private $masterpage;
    private $content;
    
    private static $body;
    
    public function __construct($content, $masterpage = '') {
        $this->content = $content;
        $this->masterpage = $masterpage;
    }
    
    public function Render() {
        if(strlen($this->masterpage) > 1) {
            $mpFile = 'masterpage/' . $this->masterpage . '.masterpage.php';
            
            if(!file_exists($mpFile)) {
                die("masterpage doesn't exist: " . $mpFile);
            }
            
            self::$body = $this->content;
            
            include_once($mpFile);
            
        } else {
            return $this->content;
        }
    }
    
    public static function RenderBody() {
        $body = self::$body;
        self::$body = '';
        
        echo $body;
    }
}