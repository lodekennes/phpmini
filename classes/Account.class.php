<?php
class Account {
    
    private static $accountVariable = '_accountVariable_';
    private static $accountLevelVariable = '_accountLevelVariable_';
    
    public static function IsLoggedIn() {
        return Request::GetSessionVariable(Account::$accountVariable) != '';
    }
    
    public static function GetUsername() {
        return self::IsLoggedIn() ? Request::GetSessionVariable(self::$accountVariable) : '';
    }
    
    public static function GetAuthLevel() {
        return self::IsLoggedIn() ? Request::GetSessionVariable(self::$accountLevelVariable) : 0;
    }
    
    public static function Login($username, $authLevel) {
        Request::SetSessionVariable(self::$accountVariable, $username);
        Request::SetSessionVariable(self::$accountLevelVariable, $authLevel);
    }
    
    public static function Logout() {
        Request::RemoveSessionVariable(self::$accountVariable);
        Request::RemoveSessionVariable(self::$accountLevelVariable);
    }
}