<?php
class TempData {
    
    private static $CookieName = 'TempData';

    public static function SetTempData($key, $value) {
        $data = self::GetCookieValue();
        
        $data[$key] = $value;
        self::SetCookieValue($data);
    }
    
    public static function GetTempData($key) {
        $data = self::GetCookieValue();
        
        return isset($data[$key]) ? $data[$key] : '';
    }
    
    public static function ClearTempData() {
        unset($_SESSION[self::$CookieName]);
    }
    
    private static function GetCookieValue() {
        return !isset($_SESSION[self::$CookieName]) ? array() : json_decode($_SESSION[self::$CookieName], true);
    }
    
    private static function SetCookieValue($finalCookie) {
        $encoded = json_encode($finalCookie);
        
        $_SESSION[self::$CookieName] = $encoded;
    }
}