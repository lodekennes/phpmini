<?php
class DatabaseReader {
    private $pdo;
    
    public function __construct($provider, $host, $dbname, $user = '', $pass = '', $options = array()) {
        $this->pdo = new PDO($provider . ':host=' . $host . ';dbname=' . $dbname, $user, $pass, $options);
    }
    
    public function ExecuteQuery($query, $args = array(), $fetchResult = false) {
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
        
        if($fetchResult) {
            return $stmt->fetchAll();
        }
    }
}