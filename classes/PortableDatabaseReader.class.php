<?php
class PortableDatabaseReader extends SQLite3 {
    public function __construct($path) {
        $this->open($path);
    }
    
    public function ExecuteQuery($query, $args = array(), $fetchResult = false) {
        
        foreach($args as $arg) {
            $query = self::str_replace_first('?', SQLite3::escapeString($arg), $query);
        }
        
        $stmt = $this->query($query);
        
        if($fetchResult) {
            $array = array();
            
            while($row=$stmt->fetchArray()){
                $array[] = $row;
                }
            return $array;
        }
    }
    
    static function str_replace_first($search, $replace, $source) {
        $explode = explode( $search, $source );
        $shift = array_shift( $explode );
        $implode = implode( $search, $explode );
        return $shift.$replace.$implode;
    }
}