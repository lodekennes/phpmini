<?php
class Request {
    private static $Method = 'aes128';
    private static $AesPassword = '1325339013253390';
    
    public static function IsPost() {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }
    
    public static function GetPostVariable($key) {
        return isset($_POST[$key]) ? $_POST[$key] : '';
    }
    
    public static function GetGetVariable($key) {
        return isset($_GET[$key]) ? $_GET[$key] : '';
    }
    
    public static function SetSessionVariable($key, $value) {
        $_SESSION[$key] = $value;
    }
    
    public static function GetSessionVariable($key) {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : '';
    }
    
    public static function RemoveSessionVariable($key) {
        unset($_SESSION[$key]);
    }
    
    public static function GetCookie($key) {
        return isset($_COOKIE[$key]) ? openssl_decrypt($_COOKIE[$key], self::$Method, self::$AesPassword, 0, self::$AesPassword) : '';
    }
    
    public static function SetCookie($key, $value) {
        setcookie($key, openssl_encrypt($value, self::$Method, self::$AesPassword, 0, self::$AesPassword));
    }
    
    public static function GetUserAgent() {
        return $_SERVER['HTTP_USER_AGENT'];
    }
    
    public static function GetIp() {
        return $_SERVER['REMOTE_ADDR'];
    }
    
    public static function Redirect($url) {
        ob_clean();
        header('Location: ' . $url);
    }
}