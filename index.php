<?php
ob_start();
session_start();

function __autoload($classname) {
    $filename = "./classes/". $classname .".class.php";
    include_once($filename);
}

$parts = split('/', $_SERVER['REQUEST_URI']);
unset($parts[0]);

$page = $parts[1] == '' ? 'index' : $parts[1];
$page = Request::GetGetVariable('page') == '' ? 'index' : Request::GetGetVariable('page');

$command = isset($parts[2]) && $parts[2] != "" ? $parts[2] : "index";

$command = Request::GetGetVariable('command') == '' ? 'index' : Request::GetGetVariable('command');

$controllerPath = 'controllers/' . $page . '.controller.php';
$controllerName = $page . 'Controller';

$viewPath = 'views/' . $page . '.view.php';
$viewName = $page . 'View';

if(!file_exists($viewPath) || !file_exists($controllerPath)) {
    die('404, the page could not be found.');
}

include_once($controllerPath);
include_once($viewPath);

$controller = new $controllerName;

if($controller->RequiredAuth() > Account::GetAuthLevel()) {
    die('unauthorized');
}

$controller->Process(array());
$controller->{$command}();

$view = new $viewName($controller);
$result = $view->{$command}();

echo $result->Render();

TempData::ClearTempData();

ob_flush();